package org.radiomkor.test.restclient;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.radiomkor.restclient.RestEntity;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TestEntity implements RestEntity<Long> {
	private Long id;
	private String name;
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
