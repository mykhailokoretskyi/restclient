package org.radiomkor.test.restclient;

import org.radiomkor.restclient.SyncRestClient;

public class TestRestClient extends SyncRestClient<TestEntity, Long> {

	TestRestClient(String url) {
		super(url, TestEntity.class);
	}
}
