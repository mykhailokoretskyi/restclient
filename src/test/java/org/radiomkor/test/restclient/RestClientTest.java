package org.radiomkor.test.restclient;

import org.junit.Test;
import org.radiomkor.restclient.RestEntity;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

public class RestClientTest {
	@Test
	public void testInit(){
		TestRestClient client = new TestRestClient("http://localhost:8080/api/rest/testEntity");
		assertEquals("Url is set within the client", client.getUrl(), "http://localhost:8080/api/rest/testEntity");
	}

	@Test
	public void testGetAll() {
		TestRestClient client = new TestRestClient("https://jsonplaceholder.typicode.com/users");
		List<TestEntity> entityList = null;
		try {
			entityList = client.get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertEquals("GetAll: returns collection", 10, entityList.size());
		assertEquals("GetAll: items are entities", true, entityList.get(0) instanceof RestEntity);
	}
}
