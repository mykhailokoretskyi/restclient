package org.radiomkor.restclient;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;


public interface Restful<T extends RestEntity<I>, I extends Serializable> {
	T get(I id) throws IOException;
	List<T> get() throws IOException;
	T create(T entity) throws IOException;
	T update(T entity) throws IOException;
	void delete(T entity) throws IOException;
	void delete(I id) throws IOException;
}
