package org.radiomkor.restclient;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public abstract class RestClient<T extends RestEntity<I>, I extends Serializable> implements Restful<T, I> {
	private String url;
	private Mode mode = Mode.SYNC;
	private final OkHttpClient client = new OkHttpClient();
	private final ObjectMapper mapper = new ObjectMapper();
	private Class<T> entityClass;
	
	public enum Mode { SYNC, ASYNC }
	
	protected RestClient(String url, Class<T> type){
		this.url = url;
		this.entityClass = type;
	}
	protected RestClient(String url, Class<T> type, Mode mode){
		this.url = url;
		this.mode = mode;
		this.entityClass = type;
	}
	
	public T get(I id) throws IOException {
		Request request = this.buildRequest(this.url + "/" + id.toString());
		Response response = this.runRequest(request);
		if (!response.isSuccessful()) {
			throw new IOException("Unsuccessful response code: " + response.code());
		}
		return null;
	}

	public List<T> get() throws IOException {
		Request request = this.buildRequest(this.url);
		Response response = this.runRequest(request);
		List<T> list = new ArrayList<T>();
		if (response != null) {
			String json = response.body().string();
			List<T> entities = mapper.readValue(json, new ArrayList<T>().getClass());
			for(Object entity : entities) {
				list.add(mapper.convertValue(entity, entityClass));
			}
		}
		return list;
	}

	public T create(T entity) throws IOException {
		Request request = this.buildRequest(this.url);
		Response response = this.runRequest(request);
		return null;
	}

	public T update(T entity) throws IOException {
		Request request = this.buildRequest(this.url + "/" + entity.getId().toString());
		Response response = this.runRequest(request);
		return null;
	}

	public void delete(T entity) throws IOException {
		this.delete(entity.getId());
	}

	public void delete(I id) throws IOException {
		Request request = this.buildRequest(this.url + "/" + id.toString());
		Response response = this.runRequest(request);
	}
	
	protected Response runRequest(Request request) throws IOException {
		throw new IOException("runRequest method must be overriden");
	}
	
	public String getUrl() {
		return url;
	}
	
	public Mode getMode() {
		return mode;
	}
	
	protected Request buildRequest(String url) {
		return new Request.Builder()
				.url(url)
				.build();
	}
	
	protected OkHttpClient getClient() {
		return this.client;
	}
}
