package org.radiomkor.restclient;

import java.io.Serializable;

public abstract class AsyncRestClient<T extends RestEntity<I>, I extends Serializable> extends RestClient<T, I> {
	protected AsyncRestClient(String url, Class<T> type) {
		super(url, type, RestClient.Mode.ASYNC);
	}
}
