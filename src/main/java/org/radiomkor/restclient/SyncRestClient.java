package org.radiomkor.restclient;

import java.io.IOException;
import java.io.Serializable;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SyncRestClient<T extends RestEntity<I>, I extends Serializable> extends RestClient<T, I> {

	protected SyncRestClient(String url, Class<T> type) {
		super(url, type);
	}
	
	@Override
	protected Response runRequest(Request request) {
		OkHttpClient client = this.getClient();
		Response response = null;
		try {
			response = client.newCall(request).execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}
}
