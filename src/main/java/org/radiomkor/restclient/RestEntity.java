package org.radiomkor.restclient;

import java.io.Serializable;

public interface RestEntity<T extends Serializable> {
	T getId();
	void setId(T id);
}
